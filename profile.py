"""Setup VNC for a novnc demo

Instructions:
Wait for the experiment to start, click on the VNC option in the node
context menu.
"""

#
# This is the install/setup/start script.
#
STARTVNC = "/bin/sh /local/repository/startvnc.sh"

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as rspec
# Import IG extensions
import geni.rspec.igext as ig

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Pick your OS.
imageList = [
    ('default', 'Default Image'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD', 'UBUNTU 18.04'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD', 'UBUNTU 20.04'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD', 'UBUNTU 16.04'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS7-64-STD',  'CENTOS 7'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS8-64-STD',  'CENTOS 8'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//FBSD114-64-STD', 'FreeBSD 11.4'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//FBSD123-64-STD', 'FreeBSD 12.3'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//FBSD130-64-STD', 'FreeBSD 13.0')]

pc.defineParameter("osImage", "Select OS for node running Xvnc",
                   portal.ParameterType.IMAGE,
                   imageList[0], imageList,
                   longDescription="Most clusters have this set of images, " +
                   "pick your favorite one.")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

# Add a raw PC to the request.
node = request.RawPC("node")
if params.osImage and params.osImage != "default":
    node.disk_image = params.osImage

#
# Create a per-experiment password, do not change its name!
# You must have line for VNC via the Portal WEB UI to work.
#
request.addResource(ig.Password("vncpasswd"))

node.addService(rspec.Execute(shell="sh", command=STARTVNC))
        
portal.context.printRequestRSpec()
