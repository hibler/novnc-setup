#!/bin/sh

SCRIPTNAME=$0
#
# Might not be on the local cluster, so need to use the urn to
# see who the actual creator is.
#
GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
if [ $? -ne 0 ]; then
    echo "ERROR: could not run geni-get user_urn!"
    exit 1
fi
if [ $USER != $GENIUSER ]; then
    sudo -u $GENIUSER $SCRIPTNAME
    exit $?
fi

OURPATH=$(dirname "$SCRIPTNAME")
INSTALL="$OURPATH/install.sh"
SETUP="$OURPATH/setup.sh"
HOMEDIR="/users/$USER"
VNCDIR="$HOMEDIR/.vnc"

OS=`uname`
if [ "$OS" = "FreeBSD" ]; then
    INSTALL="$OURPATH/install-bsd.sh"
fi

$INSTALL
if [ $? -ne 0 ]; then
    echo '$INSTALL failed'
    exit 1
fi

$SETUP
if [ $? -ne 0 ]; then
    echo '$SETUP failed'
    exit 1
fi

# sudo does not change HOME, so this ends up failing. Not sure why.
HOME=$HOMEDIR
# And start the vncserver.
$VNCDIR/vncserve.open
exit 0
