#!/bin/sh

SCRIPTNAME=$0
OURPATH=$(dirname "$SCRIPTNAME")

#
# Might not be on the local cluster, so need to use the urn to
# see who the actual creator is.
#
GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
if [ $? -ne 0 ]; then
    echo "ERROR: could not run geni-get user_urn!"
    exit 1
fi
if [ $USER != $GENIUSER ]; then
    sudo -u $GENIUSER $SCRIPTNAME
    exit $?
fi
HOMEDIR="/users/$USER"
VNCDIR="$HOMEDIR/.vnc"
VNCPSWD="$VNCDIR/passwd"

# Copy files out of the repo into home dir, otherwise create empty dir.
if [ ! -e $VNCDIR ]; then
    if [ -e "$OURPATH/.vnc" ]; then
	cp -rp $OURPATH/.vnc $HOMEDIR
	if [ $? -ne 0 ]; then
	    echo "ERROR: could not copy .vnc dir to $VNCDIR!"
	    exit 1
	fi
    else
	mkdir $VNCDIR
    fi
fi
# And the .Xdefaults file is handy.
if [ ! -e "$HOMEDIR/.Xdefaults" ]; then
    if [ -e "$OURPATH/.Xdefaults" ]; then
	cp -p $OURPATH/.Xdefaults $HOMEDIR
	if [ $? -ne 0 ]; then
	    echo "ERROR: could not copy .Xdefaults to $HOMEDIR!"
	    exit 1
	fi
    fi
fi

#
# David provided this code, from the openstack profile.
#
OURDIR=$VNCDIR

# The manifest holds the encrypted randomly generated password.
if [ ! -e $OURDIR/manifest.xml ]; then
    geni-get manifest > $OURDIR/manifest.xml
    cat $OURDIR/manifest.xml | grep -q emulab:password
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get manifest failed or no password block!"
	exit 1
    fi
fi

# Geni key decrypts the password.
if [ ! -e $OURDIR/geni.key ]; then
    geni-get key > $OURDIR/geni.key
    cat $OURDIR/geni.key | grep -q END\ .\*\PRIVATE\ KEY
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get key failed!"
	exit 1
    fi
fi

# Suck out the key from the manifest
if [ ! -e $OURDIR/encrypted_admin_pass ]; then
    cat $OURDIR/manifest.xml | perl -e '@lines = <STDIN>; $all = join("",@lines); if ($all =~ /^.+<[^:]+:password[^>]*>([^<]+)<\/[^:]+:password>.+/igs) { print $1; }' > $OURDIR/encrypted_admin_pass
fi

# And descrypt to get the password in plain text.
if [ ! -e $OURDIR/decrypted_admin_pass -a -s $OURDIR/encrypted_admin_pass ]; then
    openssl smime -decrypt -inform PEM -inkey $OURDIR/geni.key -in $OURDIR/encrypted_admin_pass -out $OURDIR/decrypted_admin_pass
    if [ $? -ne 0 ]; then
	echo "ERROR: openssl decrypt failed!"
	exit 1
    fi
fi

# And setup a vnc passwd. 
if [ ! -e $VNCDIR/passwd ]; then
    cat $OURDIR/decrypted_admin_pass | vncpasswd -f > $VNCDIR/passwd
    if [ $? -ne 0 ]; then
	echo "ERROR: vncpasswd failed!"
	exit 1
    fi
    chmod 600 $VNCDIR/passwd
fi

exit 0
