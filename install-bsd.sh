#!/bin/sh

export ASSUME_ALWAYS_YES=true
sudo -E pkg install tigervnc-server autocutsel fvwm xorg-apps xorg-fonts xorg-fonts-100dpi xfontsel
if [ $? -ne 0 ]; then
    echo 'pkg install support failed'
    exit 1
fi

exit 0
